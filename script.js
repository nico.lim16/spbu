class Person {
  constructor(name, address) {
    this.name = name;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, nama saya ${this.name} dari ${this.address}`);
  }
}

class Owner extends Person {
  constructor(name, address) {
    super(name, address);
    this.employeeList = [];
  }

  hire(name) {
    if (!name.getIsEmployee()) {
      name.setIsEmployee(1);
      this.employeeList.push(name);
      console.log(`Congratulations ${name.getName()}, you're now an employee`);
    } else {
      console.log(`Sorry, ${name.getName()} is already an employee.`);
    }
  }

  fire(name) {
    for (let i = 0; i < this.employeeList.length; i++) {
      if (this.employeeList[i] == name) {
        console.log(`${this.employeeList[i].getName()} is no longer working here.`)
        this.employeeList.splice(i, 1);
        name.setIsEmployee(0);
        break;
      } else {
        console.log(`Sorry, ${name.getName()} is not an employee here.`);
      }
    }
  }

  check() {
    console.log(`You have ${this.employeeList.length} employee(s)`);
    for (let i = 0; i < this.employeeList.length; i++) {
      console.log(`${i + 1}. ${this.employeeList[i].name}`);
    }
  }
}

class Employee extends Person {
  constructor(name, address) {
    super(name, address);
    this.isEmployee = 0;
    this.discount = 0;
  }

  refillFuel(name, amount, gateNumber) {
    if (this.isEmployee) {
      if (gateNumber.getRemainingFuel() >= amount) {
        if (name.getFuelAtTank() + amount <= name.getMaxFuel()) {
          name.setFuelAtTank(amount);
          name.setTotalTransaction();
          gateNumber.setRemainingFuel(-amount);
          console.log(`Refill successfull. You now have ${name.getFuelAtTank()}L in your tank`);
          console.log(`Your total transaction is ${(1 - (this.getDiscount(name, amount, gateNumber) / 100)) * (amount * gateNumber.getFuelPrice())}`)
        } else {
          console.log(`Sorry, you can't refill that much. Please take a ride or reduce the amount of fuel you want to buy`);
        }
      } else {
        console.log('Sorry, this gate runs out of fuel. Please wait for the gate to be refilled or move to the other gate.');
      }
    } else {
      console.log(`Sorry, you're not an Employee. Please contact Taslin.`);
    }
  }

  getDiscount(name, amount, gateNumber) {
    if (name.getTotalTransaction() >= 3) {
      if (amount * gateNumber.getFuelPrice() < 100000) {
        if (amount * gateNumber.getFuelPrice() < 20000) {
          return 0;
        } else {
          return 2.5;
        }
      } else {
        return 10;
      }
    } else {
      return 0;
    }
  }

  refillGate(gateNumber, amount) {
    if (this.isEmployee) {
      gateNumber.setRemainingFuel(amount);
    } else {
      console.log(`Sorry, you are not an employee.`);
    }
  }

  setIsEmployee(num) {
    this.isEmployee = num;
  }

  getIsEmployee() {
    return this.isEmployee;
  }

  getName() {
    return this.name;
  }
}

class Customer extends Person {
  constructor(name, address, fuelAtTank, maxFuel, mileage) {
    super(name, address);
    this.fuelAtTank = fuelAtTank;
    this.maxFuel = maxFuel;
    this.mileage = mileage;
    this.totalTransaction = 0;
  }
  
  ride(distance) {
    if (((this.getMileage() * this.getFuelAtTank()) - distance) >= 0) {
      this.setFuelAtTank(-(distance / this.getMileage()));
      console.log(`You ride ${distance}km. Your fuel can last another ${this.getFuelAtTank() * this.getMileage()} km.`);
    } else {
      console.log(`Sorry, you dont have enough fuel to go that far.`);
    }
  }

  setFuelAtTank(num) {
    this.fuelAtTank += num;
  }

  getFuelAtTank() {
    return this.fuelAtTank;
  }

  getMaxFuel() {
    return this.maxFuel;
  }

  getMileage() {
    return this.mileage;
  }

  setTotalTransaction() {
    this.totalTransaction += 1;
  }

  getTotalTransaction() {
    return this.totalTransaction;
  }
}

class Gate {
  constructor(number, remainingFuel) {
    this.number = number;
    this.remainingFuel = remainingFuel;
    this.fuelPrice = 10000;
  }

  getNumber() {
    return this.number;
  }

  setRemainingFuel(amount) {
    this.remainingFuel += amount;
  }

  getRemainingFuel() {
    return this.remainingFuel;
  }

  getFuelPrice() {
    return this.fuelPrice;
  }
}

let Taslin = new Owner('Taslin', 'Jakarta');
let Budi = new Customer('Budi', 'Bangka', 10, 30, 10);
let Anton = new Customer('Anton', 'Bogor', 2, 24, 12);
let Toni = new Customer('Toni', 'Depok', 6, 36, 8);
let Akmal = new Employee('Akmal', 'Bekasi');
let Fauzan = new Employee('Fauzan', 'Priuk');
let Tuti = new Employee('Tuti', 'Yogya');
let Gate1 = new Gate(1, 40);
let Gate2 = new Gate(2, 50);

Taslin.hire(Tuti);
Tuti.refillFuel(Budi, 20, Gate1);
Budi.ride(400);
Budi.ride(300);
Tuti.refillFuel(Budi, 30, Gate1);
Tuti.refillFuel(Budi, 30, Gate2);
Taslin.hire(Fauzan);
Taslin.hire(Akmal);
Taslin.check();